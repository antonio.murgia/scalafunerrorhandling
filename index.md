---
title: "Fun-scala"
separator: "\n---\n"
verticalSeparator: "\n--\n"
theme : "solarized"
revealOptions:
  transition: "slide"
  highlightTheme: "dracula"
  logoImg: "assets/logo.png"
  slideNumber: "true"
---

# Fun Error Handling in Scala

---

# ➜ whoami

![Me](assets/me.jpeg)

--

# Antonio Murgia

- Big Data Engineer @ AgileLab
- (dis)functional programmer by chance
- Outdoor life lover
- Fattest crossfitter you'll ever know
- Organizer of Bologna Big Data Meetup
- 🐦 tmnd1991

--

# Agenda

- Introduction
- Either
- Complex programs
- Life is not sequential
- Intuitions
- Conclusions

---

## Motivation

- Scala is widely used as a better Java ™️, that's not fair!
- Scala has one of the most advanced type systems
- I do not know any other programming language
- I do not want to write another monad talk/tutorial
- I like memes and love to show them to my friends

--

## The stack 

- scala 2.11 (because Spark is still in 2014)
- cats 🐱 2.0.0 (cross-compiled 2.11/2.12/2.13)
- usage of implicits and typeclasses

--

### The type system is your friend

![Type System 1](assets/typesystem.png)
![Type System 2](assets/typesystem2.png)

--

## Java

### Checked exceptions! 1/2

```java
String print3lines(String f) {
  FileReader file = new FileReader(f);
  BufferedReader fileInput = new BufferedReader(file);
  String s = "";
  // Print first 3 lines of file "test.txt"
  for (int counter = 0; counter < 3; counter++)
    s += fileInput.readLine() + "\n";
  return s;
}
```

--

### Checked exceptions! 2/2

```bash
Unhandled exception: java.io.IOException: 
must be caught or declared to be thrown
```

--

![Errors](assets/errorsMF.jpg)

--

### Solution:

```java
String print3lines(String f) throws IOException {
  FileReader file = new FileReader(f); 
  BufferedReader fileInput = new BufferedReader(file); 
  String s = "";
  // Print first 3 lines of file "test.txt" 
  for (int counter = 0; counter < 3; counter++)  
    s += fileInput.readLine() + "\n"; 
  return s;
}
```

--

### How does Java handle it?

> "Fatta la legge trovato l'inganno" (IT)
> "Every law has a loophole" (EN)

Java has unchecked exceptions that do not "annoy" the programmer and tend to be used everywhere.

--

### 👎Common practice💩

```java
String print3lines(String f) {
  try {
    FileReader file = new FileReader(f); 
    BufferedReader fileInput = new BufferedReader(file); 
    String s = "";
    // Print first 3 lines of file "test.txt" 
    for (int counter = 0; counter < 3; counter++)  
      s += fileInput.readLine() + "\n"; 
    return s;
  } catch (IOException e) {
    throw new RuntimeException(e);
  }
}
```

--

**DON'T DO THAT**

**EVERY TIME YOU DO IT, <br/> A KITTEN DIES (somewhere)**

--

## Java

| Description | PRO | CONS |
| ----------- | --- | ---- |
| handle vs delegate explicitly | ✅ | |
| language built-in, not customizable | | ❌ |
| does not reflect in the type system | | ❌ |
| exceptions are `Unconditional Jump`s | | ❌ |
| there is a very big loop-hole | | ❌ |

--

## And Scala?

- only unchecked exceptions

**BUT**

- a powerful type system

**LET'S LEVERAGE IT!**

--

## ADT

**A**lgebraic **D**ata **T**ypes

- `×` Product Types
- `+` Sum Types

--

## Types have domains

| type | domain |
| ---- | ----------- |
| `Nothing` | `∅` |
| `Unit` | `{()}` |
| `Boolean` | `{true, false}` |
| `Int` | `[-2³¹, 2³¹)` |

--

## Product types 
(aka `case class` or `Tuple`)

They do what you expect them to do:

| type | domain |
| ---- | ----------- |
| `Boolean` | `{true, false}` |
| `Int` | `[-2³¹, 2³¹)` |
| `(Int, Boolean)` | `[-2³¹, 2³¹) × {true, false}` |

--

## Sum types 

They do what you expect them to do:

```scala
sealed trait SwitchState
case class  On(intensity: Int) extends SwitchState
case object Off                extends SwitchState
case object Unkwnown           extends SwitchState
```

| type | domain |
| ---- | ----------- |
| `On` | `[-2³¹, 2³¹)` |
| `Off` | `{Off}` |
| `Unknown` | `{Unknown}` |
| `SwitchState` | `[-2³¹, 2³¹) + {Off} + {Unknown}` |

--

## Why is this important?

- You keep "domain"s of every function in your head while reasoning about code
- Smaller domains, easier reasoning, built-in docs
- If you want to stress this, have a look at [refined](https://github.com/fthomas/refined)

--

## Checked exceptions (again)

Checked exceptions are "similar" to sum types

```java
String print3lines(String f) throws IOException
```

```scala
sealed trait StringOrError
case class S(s: String)        extends StringOrError
case class IOE(e: IOException) extends StringOrError

def print3lines(f: String): StringOrError
```

--

## Handling Errors

IDE/Compiler Support 

```scala
print3lines("file.txt") match {
  case S(s)   => println(s)
}
```

```shell
[warn] Main.scala:12:39: match may not be exhaustive.
[warn] It would fail on the following inputs: IOE(_)
[warn]   print3lines("file.txt") match {
[warn]   ^
```

```scala
scalacOptions ++= Seq(
  "-Xfatal-warnings"
)
```

--

## Handling Errors

IDE/Compiler Support 

![Exhaustive](assets/exhaustive.png)

```scala
print3lines("file.txt") match {
  case S(s)   => println(s)
  case IOE(e) => handleTheError(e)
}
```

--

**But this is a lot of boiler plate!**

--

## So we have these fantastic `sum` types

- `Option[A]`
- `Either[A, B]`

--

## `Option[A]`

```scala
sealed abstract class Option[+A]()
case object None                     extends Option[Nothing]
final case class Some[+A](val x : A) extends Option[A]
```

--

## `Either[A, B]`

```scala
sealed abstract class Either[+A, +B]() 
final case class Left[+A, +B](val a : A)  extends Either[A, B]
final case class Right[+A, +B](val b : B) extends Either[A, B]
```

--

## Handling Errors

```scala
type StringOrError = Either[IOException, String]

def print3lines(f: String): StringOrError

print3lines("file.txt") match {
  case Right(s)   => println(s)
  case Left(e)    => handleTheError(e)
}
```

---

# A tour into `Either`

--

## map

Left biased:

```
def map[X](f: (A => X)) : Either[X, B]
```

or Right biased (default since scala 2.12)

```
def map[X](f: (B => X)) : Either[A, X]
```

```scala
either.left.map{...}
either.right.map{...}
```

--

## flatMap

Left biased:

```
def flatMap[X](f : (A => Either[X, B])) : Either[X, B]
```

or Right biased (default since scala 2.12)

```
def flatMap[X](f: (B => Either[A, X])) : Either[A, X]
```

```scala
either.left.flatMap{...}
either.right.flatMap{...}
```

--

## get

**DON'T DO THAT**

**ANOTHER KITTEN DIES**

```scala
Left(3).right.get
java.util.NoSuchElementException: Either.right.value on Left
  at scala.util.Either$RightProjection.get(Either.scala:453)
```

--

## The ways "out"

- `match`

- `fold`

```scala
def fold[X](fa: A => X, fb: B => X) : X
```

--

![Heisenberg](assets/Heisenberg.jpg)

---

## But my programs are more complex 

--

## Complex Program

Given a list of integers, if the sum of all elements is higher than 100, 
divide the sum of all elements by the head of the list, otherwise, multiply
it by the head of the list

--

## First approach

```scala
def head(list: List[Int]): Int = list.head
def sum(list: List[Int]): Int = list.sum
def program(list: List[Int]): Int = {
  val s = sum(list)
  if (s > 100) s / head(list)
  else s * head(list)
}
```

--

## The caller perspective

- can this fail? <br/>`def program(list: List[Int]): Int`
- what are the error cases?

--

### Another approach

The Error Algebra

```scala
sealed trait ProgramError
case object EmptyList extends ProgramError
case class DivisionByZero(dividend: Int) extends ProgramError
```

--

### Another Approach - v1

```scala
def head(list: List[Int]): Either[ProgramError, Int] = list match {
  case head :: _ => Right(head)
  case Nil       => Left(EmptyList)
}
def sum(list: List[Int]): Int = list.sum
def div(dividend: Int, divisor: Int): Either[DivisionByZero, Int] =
  if (divisor == 0) Left(DivisionByZero(dividend))
  else Right(dividend / divisor)

def program(l: List[Int]): Either[ProgramError, Int] = head(l) match {
    case Left(a) => Left(a)
    case Right(h) =>
      val s = sum(l)
      if (s > 100) div(s, h)
      else Right(s * h)
  }
```

--

### Another Approach - v2

```scala
def sum(list: List[Int]): Int = list.sum
def head(list: List[Int]): Either[ProgramError, Int] =
  list.headOption.toRight(EmptyList)
def div(dividend: Int, divisor: Int): Either[DivisionByZero, Int] =
  if (divisor == 0) Left(DivisionByZero(dividend))
  else Right(dividend / divisor)

def program(list: List[Int]): Either[ProgramError, Int] = {
  def op(s: Int, h: Int) = if (s > 100) div(s, h) else Right(s * h)
  for {
    h <- head(list).right
    s = sum(list)
    res <- op(s, h).right
  } yield res
}
```

--

## The caller perspective

- can this fail? <br/>`def program(list: List[Int]): Either[ProgramError, Int]`
- what are the error cases?
  - `EmptyList`
  - `DivisionByZero`

--

### Another example: A lot of functions to apply

Example, try to implement `f`:

```scala
type E = String
case class TagPoint(name: String, lat: Double, alt: Int, long: Double)
def f1(a: Int): Either[E, Double]                                    = ???
def f2(a: Double, b: Double): Either[E, String]                      = ???
def f3(a: Double, b: Double, c: Int, s: String): Either[E, TagPoint] = ???
def f(lat: Int, long: Int, alt: Int): Either[E, TagPoint] = ???
```

--

### Logical view (without error handling)

```scala
def f(lat: Int, long: Int, alt: Int): Either[E, TagPoint] = {
  val dLat = f1(lat)
  val dLong = f1(long)
  val name = f2(dLat, dLong)
  f3(dLat, dLong, alt, name)
}
```

--

### Match

```scala
def f(lat: Int, long: Int, alt: Int): Either[E, TagPoint] =
  f1(lat) match {
    case Right(rLat) =>
      f1(long) match {
        case Right(rLong) =>
          f2(rLat, rLong) match {
            case Right(tagName) => f3(rLat, rLong, alt, tagName)
            case Left(a)        => Left(a)
          }
        case Left(a) => Left(a)
      }
    case Left(a) => Left(a)
  }
```

--

![KillMe](assets/killme.png)

--

### Flatmap

```scala
def f(lat: Int, long: Int, alt: Int): Either[E, TagPoint] =
  f1(lat).right.flatMap{ rLat =>
    f1(long).right.flatMap{ rLong =>
      f2(rLat, rLong).right.flatMap{ tagName =>
        f3(rLat, rLong, alt, tagName)
      }
    }
  }
```

--

![Hadouken](assets/hadouken.jpg)


--

### for yield 💘

This is just syntactic sugar over the flatMap solution:

```scala
def f(lat: Int, long: Int, alt: Int): Either[E, TagPoint] =
  for {
    rLat    <- f1(lat).right
    rLong   <- f1(long).right
    tagName <- f2(rLat, rLong).right
    result  <- f3(rLat, rLong, alt, tagName).right
  } yield result
```

--

# What 🐱 brings you

--

### Ergonomic syntax

```scala
import cats.syntax.either._

3.asRight[String]     // Either[String, Int] 😀
Right(3)              // Right[Nothing, Int] 🤔
Right[String, Int](3) // Right[String, Int]  🤔
```

--

### Ergonomic syntax

```scala
import cats.syntax.either._

3.asRight[String].bimap(
  (_: String) => "Error: -1",
  (i: Int)    => i * 2
)

3.asRight[String].left.map(
  (_: String) => "Error: -1"
).right.map(
  (i: Int)    => i * 2
)
```

--

### Handy data structures:

```scala
cats.data.Chain
```
> Chain is a data structure that allows constant time prepending and appending

--

### Refined Data structures:

```scala
cats.data.NonEmptyMap
cats.data.NonEmptySet
cats.data.NonEmptyList
cats.data.NonEmptyChain
```

--

### ...and a reasoining framework called
### functional programming which I won't cover 🐱

---

## Life is not sequential

You don't always have operations that must be performed sequentially.

An example is the application of `f1` to both `lat` and `long` parameters.

We can apply those two functions in either order (since they are indipendent) and we want to collect both error cases (if they happen)

--

### This is not pretty (Jon)

```scala
def f(lat: Int, long: Int, alt: Int): Either[E, TagPoint] =
  (f1(lat), f1(long)) match {
    case (Left(msg1), Left(msg2)) => Left(msg1 + "\n" + msg2)
    case (Left(msg1), _)          => Left(msg1)
    case (_, Left(msg2))          => Left(msg2)
    case (Right(rLat), Right(rLong)) =>
      for {
        tagName <- f2(rLat, rLong).right
        result  <- f3(rLat, rLong, alt, tagName).right
      } yield result
  }
```

--

### Validated[E, A]

```scala
sealed abstract class Validated[+E, +A]
final case class Valid[+A](a: A)   extends Validated[Nothing, A]
final case class Invalid[+E](e: E) extends Validated[E, Nothing]
```

Very similar to Either but with different "rules".

--

### Useful type aliases of Validated

```scala
type ValidatedNel[+E, +A] = Validated[NonEmptyList[E], A]
type ValidatedNec[+E, +A] = Validated[NonEmptyChain[E], A]
```

--

### Useful methods and functions

```scala
import cats.data.Validated

Validated[E, A].toEither // Either[E, A]

import cats.either.syntax._

Either[E, A].toValidated // Validated[E, A]
Either[E, A].toValidatedNec // ValidatedNec[E, A] -> Validated[NonEmptyChain[E], A]
Either[E, A].toValidatedNel // ValidatedNel[E, A] -> Validated[NonEmptyList[E], A]
```

--

### Useful methods and functions

```scala
import cats.data.Validated
import cats.syntax.validated._

3.valid            // Validated[Nothing, Int]
3.valid[String]    // Validated[String, Int]
3.validNel[String] // ValidatedNel[String, Int]
3.validNec[String] // ValidatedNec[String, Int]
```

--

### Using Validated

```scala
import cats.syntax.either._
import cats.syntax.apply._

def applyF1ToBoth(lat: Int, long: Int): Either[E, (Double, Double)]
```

--

### Using Validated

```scala
import cats.syntax.either._
import cats.syntax.apply._

def applyF1ToBoth(lat: Int, long: Int): Either[E, (Double, Double)] =
  (f1(lat).toValidatedNec, f1(long).toValidatedNec).mapN {
    case (rLat, rLong) => rLat -> rLong
  } // ValidatedNec[E, (Double, Double)]
```

--

### Using Validated

```scala
import cats.syntax.either._
import cats.syntax.apply._

def applyF1ToBoth(lat: Int, long: Int): Either[E, (Double, Double)] =
  (f1(lat).toValidatedNec, f1(long).toValidatedNec).mapN {
    case (rLat, rLong) => rLat -> rLong
  }.toEither // Either[NonEmptyChain[E], (Double, Double)]
```

--

### Using Validated

```scala
import cats.syntax.either._
import cats.syntax.apply._

def applyF1ToBoth(lat: Int, long: Int): Either[E, (Double, Double)] =
  (f1(lat).toValidatedNec, f1(long).toValidatedNec).mapN {
    case (rLat, rLong) => rLat -> rLong
  }.toEither.leftMap(_.reduceLeft(_ + "\n" + _))
```

--

### Using Validated

```scala
def f(lat: Int, long: Int, alt: Int): Either[E, TagPoint] = {
  for {
    tuple         <- applyF1ToBoth(lat, long)
    (rLat, rLong) = tuple
    tagName       <- f2(rLat, rLong).right
    result        <- f3(rLat, rLong, alt, tagName).right
  } yield result
}
```

--

### Remarks

- Less verbose than the `match` approach
- "Scales" up to 22 elements of any combined type
- More than 22 is not a Tuple anymore 😏

---

# Intuitions

--

`Validated[E, A]` and `Either[E, A]` are "cousins"

They both express a computation that fails or succeeds in some way

--

The main difference is:

- with `Either` you stop at the first `Left`
- with `Validated` you go on aggregating `Invalid`

--

## This aggregation 
## s\*\*t ain't free

--

In fact you cannot perform `mapN` trick in **all** cases

```scala
  val a = 1.validNel[Throwable]
  val b = (new RuntimeException).invalid[Int]
  val c = (new RuntimeException).invalid[Int]
  (a, b, c).mapN((ra, rb, rc) => ra + rb + rc)
  // Error: could not find implicit value for parameter 
  // semigroupal: Semigroupal[cats.data.Validated[Throwable,A]]
```

--

## Why tho?

![Why](assets/y.png)

--

### What's a semigroupal?

> Combine an `F[A]` and an `F[B]` into an `F[(A, B)]` that maintains the effects of both `fa` and `fb`

--

### Why `Either` always has a Semigroupal while `Validated` doesn't?

--

#### Why `Either` always has a Semigroupal while `Validated` doesn't?

- With `Either` you stop at the first "error", you don't need any logic to aggregate errors

- With `Validated` you have to "aggregate" the error part

--

```
// Error: could not find implicit value for parameter 
// semigroupal: Semigroupal[cats.data.Validated[Throwable,A]]
```

It is actually saying:

How the f\*\*k do I do:

`Throwable |+| Throwable`

### ?

--

There is no "universal" meaning of

`Throwable` + `Throwable`

While it's there for:

`NonEmptyChain[A] |+| NonEmptyChain[A]`

--

## This concept of "merge" has a name

--

# Semigroup

A "type" has a semigroup if it respects a rule (also known as law).

Associativity:

```scala
(a |+| b) |+| c == a |+| (b |+| c)
```

--

![Semigroup](assets/semigroup.jpg)

--

So whenever you define a custom semigroup, be sure to check that is really a semigroup.

--

Aggregate things -> `Semigroup`

Which in `cats` is a typeclass: `Semigroup[A]`

--

`cats` has semigroups for a lot of types:

```scala
class StringSemigroup extends Semigroup[String] {
  def combine(x: String, y: String): String = x + y
}

class ListSemigroup[A] extends Semigroup[List[A]] {
  def combine(x: List[A], y: List[A]): List[A] = x ::: y
}

class MapSemigroup[A, B: Semigroup] extends Semigroup[Map[A. B]] {
  def combine(x: Map[A, B], y: Map[A, B]): Map[A. N] = {
    ...
  }
}
```

--

With the power of typeclasses you can have a semigroup for every type you need </br> (custom or imported)

ad-hoc polymorphism ❤️

--

For the most adventurous ones, the concepts that we just see correspond to the name of:

- Semigroup
- Monoid
- Applicative
- Functor
- Monad
- MonadError
- ApplicativeError

Yeah, I know the names are scary... I personally go with intuitions and then in the end figure out the theory

---

# Take aways

--

- These "things" are real
- you end up dealing with these "things" every day
- you just don't model them this way
  - or don't model them at all
- If you do this, you signature will speak to the world

--

>But I usually obtain the same things "manually"

--

This means you need them, but you have your custom "rules" that only your team knows. <br/>`cats` is "universally accepted"

--

> So now every function will return an `Either` or a `Validated` and `Exception`s are never thrown?

--

No, my personal advice, is:

- use only `Either` in signatures
  - transform them to `Validated` when you need to `mapN`

--

No, my personal advice, is:

- catch only business errors, make "bubble up" exceptions
  - exceptions should be unexpected, </br>let me pass this term: transient

--

### So you've lied to me!

I didn't, we will see how to handle REAL `exception`s in a principled and typesafe manner, but not today.

The thing is:
  - what we have seen so far should not deal with "side-effects"
  - it's good for business logic, not for interacting with the world

--

What our business logic **should** look like:

![pure](assets/pure.jpeg)

--

For that there will be another "workshop"

Pointers for the impatient:

- cats-effect 🐱🧨
- ZIO 💪💣

---

### Hope this wasn't

![](assets/wasnot1.jpeg)

--

Follow @impurepics for more

--

# Questions ?
